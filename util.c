
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/select.h>
#include <sys/time.h> 
#include "util.h"

void clear_terminal(void) {
	printf("\033c");
}

long get_microseconds(void) {
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

void gotoxy(int x,int y) {
    printf("%c[%d;%df",0x1B,y,x);
}

double get_terminal_columns(void) {
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	return w.ws_col;
}

double get_terminal_rows(void) {
	struct winsize w;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
	return w.ws_row;
}

int get_char_without_echoing(void) {
    struct termios oldattr, newattr;
    tcgetattr(STDIN_FILENO, &oldattr);
    newattr = oldattr;
    newattr.c_lflag &= ~( ICANON | ECHO );
    tcsetattr(STDIN_FILENO, TCSANOW, &newattr);
    char input = getchar();
    tcsetattr(STDIN_FILENO, TCSANOW, &oldattr);
    return input;
}

