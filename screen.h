
#ifndef SCREEN_H
#define SCREEN_H

#define EMPTY_CHAR (char)31

typedef struct {
	int width;
	int height;
	int buffer_length;
	char buffer[];
} Screen;

void set_char_on_screen(Screen* screen, char c, int x, int y);
void set_unicode_on_screen(Screen* screen, char* c, int x, int y);
Screen* create_screen(int width, int height);
void destroy_screen(Screen* screen);
void print_screen(Screen* screen);

#endif

