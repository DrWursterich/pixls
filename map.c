
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "map.h"
#include "geometry.h"

Map* create_map() {
	int width = 16, height = 16;
	int buffer_length = (width + 1) * height;
	Map* map_pointer = malloc(sizeof(Map) + buffer_length * sizeof(char));
	if (!map_pointer) {
		perror("failed to allocate memory for the map buffer");
		exit(EXIT_FAILURE);
	}
	map_pointer->width = width;
	map_pointer->height = height;
	map_pointer->buffer_length = buffer_length;
	map_pointer->buffer[buffer_length];
	strcpy(map_pointer->buffer,
		"################\n"
		"#..............#\n"
		"#..............#\n"
		"#..#...######..#\n"
		"#..#...#....#..#\n"
		"#..#........#..#\n"
		"#..#........#..#\n"
		"#..#####..###..#\n"
		"#..............#\n"
		"#..............#\n"
		"#..............#\n"
		"#..............#\n"
		"#.#............#\n"
		"#..............#\n"
		"#..............#\n"
		"################");
	return map_pointer;
}

void destroy_map(Map* map) {
	free(map);
}

char get_map_tile_of_position(Map* map, Position position) {
	return map->buffer[(int)position.x + (map->width + 1) * (int)position.y];
}

bool position_is_on_map(Position position, Map* map) {
	return position.x >= 0
			&& position.y >= 0
			&& position.x < map->width
			&& position.y < map->height;
}

