
#ifndef GEOMETRY_H
#define GEOMETRY_H

typedef struct {
	float x;
	float y;
} Position;

typedef struct {
	float direction;
	float length;
} Vector;

float get_distance_between_positions(Position p1, Position p2);
Position apply_vector_to_position(Vector vector, Position position);

#endif

