
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>

#include "geometry.h"
#include "map.h"
#include "screen.h"
#include "util.h"
#include "pixls.h"

static bool input_thread_running = false;
static char last_input = (char)0;

void draw_map_and_player_into_screen_buffer(Map* map, Entity player, Screen* screen) {
	int horizontal_offset = screen->width - map->width;
	for (int x=0; x < map->width; x++) {
		for (int y=0; y < map->height; y++) {
			set_char_on_screen(screen, map->buffer[x + (map->width + 1) * y], horizontal_offset + x, y);
		}
	}
	set_char_on_screen(screen, MAP_PLAYER_REPRESENTATION, horizontal_offset + (int)player.position.x, (int)player.position.y);
}

void move_entity_on_map(Entity* entity, Map* map, float direction, float distance) {
	Vector movement_vector = {direction, distance};
	Position newPosition = apply_vector_to_position(movement_vector, entity->position);
	if (position_is_on_map(newPosition, map)
			&& get_map_tile_of_position(map, newPosition) != MAP_TILE_WALL) {
		entity->position = newPosition;
	}
}

bool is_position_a_corner(Position position) {
	return sqrt(
				powf(round(position.x) - position.x, 2)
				+ powf(round(position.y) - position.y, 2)
			) < CORNER_SIZE;
}

Ray_cast_result cast_ray(Position position, float direction, Map* map) {
	Vector vector = {direction, 0};
	Position test_position;
	while (vector.length < RENDER_DISTANCE) {
		vector.length += 0.1f;
		test_position = apply_vector_to_position(vector, position);
		if (!position_is_on_map(test_position, map)) {
			break;
		}
		if (get_map_tile_of_position(map, test_position) == MAP_TILE_WALL) {
			if (ENABLE_CORNER_DETECTION) {
				return (Ray_cast_result){vector.length, is_position_a_corner(test_position)};
			}
			return (Ray_cast_result){vector.length, false};
		}
	}
	return (Ray_cast_result){RENDER_DISTANCE, false};
}

char* get_shade_by_distance(float distance) {
	if (distance <= RENDER_DISTANCE / 4.0f) {
		return "\u2588";
	}
	if (distance < RENDER_DISTANCE / 3.0f) {
		return "\u2593";
	}
	if (distance < RENDER_DISTANCE / 2.0f) {
		return "\u2592";
	}
	if (distance < RENDER_DISTANCE) {
		return "\u2591";
	}
	return (char[]){' ', EMPTY_CHAR, EMPTY_CHAR};
}

void* run_input_thread() {
	input_thread_running = true;
	while (input_thread_running != false) {
        last_input = get_char_without_echoing();
	}
	pthread_exit(NULL);
}

int main(int argc, char** argv) {
	bool running = true;
	Screen* screen = create_screen(get_terminal_columns(), get_terminal_rows() - 2);
	Entity player = {8.5f, 8.5f, 0.0f};
	Map* map = create_map();
	clear_terminal();
	pthread_t input_thread;
	pthread_create(&input_thread, NULL, run_input_thread, NULL);
	long last_iteration_start = get_microseconds();
	long current_iteration_start;
	long time_since_last_iteration;
	do {
		current_iteration_start = get_microseconds();
		time_since_last_iteration = (float)(current_iteration_start - last_iteration_start);
		if (time_since_last_iteration < MIN_FRAME_INTERVAL) {
			usleep(MIN_FRAME_INTERVAL - time_since_last_iteration);
			current_iteration_start = get_microseconds();
			time_since_last_iteration = (float)(current_iteration_start - last_iteration_start);
		}
		gotoxy(0, get_terminal_rows() - screen->height - 1);
		printf("\033[2Kfps:%5i", (int)ceil(1000000.0f / time_since_last_iteration));
		last_iteration_start = current_iteration_start;
		if (last_input != (char)0) {
			char input = last_input;
			last_input = (char)0;
			if (input == 'q') {
				running = false;
			} else if (input == 'D') { //Left-Key
				player.direction += 0.1;
			} else if (input == 'C') { //Right-Key
				player.direction -= 0.1;
			} else if (input == 'w') {
				move_entity_on_map(&player, map, player.direction, PLAYER_MOVE_SPEED);
			} else if (input == 'a') {
				move_entity_on_map(&player, map, player.direction + M_PI / 2, PLAYER_MOVE_SIDEWAYS_SPEED);
			} else if (input == 's') {
				move_entity_on_map(&player, map, player.direction, -1 * PLAYER_MOVE_SPEED);
			} else if (input == 'd') {
				move_entity_on_map(&player, map, player.direction - M_PI / 2, PLAYER_MOVE_SIDEWAYS_SPEED);
			}
		}
		gotoxy(0, get_terminal_rows() - screen->height);
		for (float column = 0.0f; column < screen->width; column++) {
			float fov_border = (player.direction + FOV / 2.0f);
			float horizontal_percentage = column / screen->width;
			float direction = fov_border - horizontal_percentage * FOV;
			Ray_cast_result ray_cast_result = cast_ray(player.position, direction, map);
			float distance_to_wall = ray_cast_result.distance;
			int ceiling = screen->height * (0.5f - 1 / distance_to_wall);
			int floor = screen->height - ceiling;
			for (int y = 0; y < screen->height; y++) {
				if (y < ceiling) {
					set_char_on_screen(screen, ' ', column, y);
				} else if (y >= ceiling && y <= floor) {
					if (ray_cast_result.is_corner) {
						set_char_on_screen(screen, ' ', column, y);
					} else {
						set_unicode_on_screen(screen, get_shade_by_distance(distance_to_wall), column, y);
					}
				} else {
					float floor_distance = 1.0f - (((float)y - screen->height / 2.0f) / (screen->height / 2.0f));
					char shade = floor_distance < 0.2
							? '#'
							: floor_distance < 0.4
								? 'x'
								: floor_distance < 0.6
									? '+'
									: floor_distance < 0.8
										? '.'
										: ' ';
					set_char_on_screen(screen, shade, column, y);
				}
			}
		}
		draw_map_and_player_into_screen_buffer(map, player, screen);
		print_screen(screen);
	} while (running == true);
	destroy_map(map);
	destroy_screen(screen);
	input_thread_running = false;
	pthread_join(input_thread, NULL);
	gotoxy(0, get_terminal_rows());
	exit(EXIT_SUCCESS);
}

