
CC=gcc

pixls: pixls.c pixls.h geometry.o map.o screen.o util.o
	$(CC) -o $@ -g $^ -lm -lpthread
	$(RM) *.o

geometry.o: geometry.c geometry.h
	$(CC) -o $@ -g -c $<

map.o: map.c map.h
	$(CC) -o $@ -g -c $<

screen.o: screen.c screen.h
	$(CC) -o $@ -g -c $<

util.o: util.c util.h
	$(CC) -o $@ -g -c $<

clean:
	$(RM) $(TARGET)

