
#include <stdio.h>
#include <stdlib.h>

#include "screen.h"

void set_char_on_screen(Screen* screen, char c, int x, int y) {
	int buffer_index = x * 3 + y * (screen->width + 1) * 3;
	screen->buffer[buffer_index] = c;
	screen->buffer[buffer_index + 1] = EMPTY_CHAR;
	screen->buffer[buffer_index + 2] = EMPTY_CHAR;
}

void set_unicode_on_screen(Screen* screen, char* c, int x, int y) {
	int buffer_index = x * 3 + y * (screen->width + 1) * 3;
	for (short i = 0; i < 3; i++) {
		screen->buffer[buffer_index + i] = c[i];
	}
}

Screen* create_screen(int width, int height) {
	int buffer_length = (width + 1) * 3 * height;
	Screen* screen_pointer = malloc(sizeof(Screen) + buffer_length * sizeof(char));
	if (!screen_pointer) {
		perror("failed to allocate memory for the screen buffer");
		exit(EXIT_FAILURE);
	}
	screen_pointer->width = width;
	screen_pointer->height = height;
	screen_pointer->buffer_length = buffer_length;
	screen_pointer->buffer[buffer_length];
	for (int i = 0; i < screen_pointer->height; i++) {
		set_char_on_screen(screen_pointer, '\n', screen_pointer->width, i);
	}
	return screen_pointer;
}

void destroy_screen(Screen* screen) {
	free(screen);
}

void print_screen(Screen* screen) {
	printf("%.*s\033[2K", screen->buffer_length, screen->buffer);
}

