
#ifndef UTIL_H
#define UTIL_H

void clear_terminal(void);

long get_microseconds(void);

void gotoxy(int x,int y);

double get_terminal_columns(void);

double get_terminal_rows(void);

int get_char_without_echoing(void);

#endif

