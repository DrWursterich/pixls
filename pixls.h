
#ifndef PIXLES_H
#define PIXLES_H

#include <stdbool.h>

#include "geometry.h"
#include "map.h"
#include "screen.h"

#define FOV M_PI / 4
#define FPS_LIMIT 30.0f
#define MIN_FRAME_INTERVAL 1000000.0f / FPS_LIMIT
#define RENDER_DISTANCE 16.0f
#define MAP_PLAYER_REPRESENTATION 'P'
#define PLAYER_MOVE_SPEED 0.5f
#define PLAYER_MOVE_SIDEWAYS_SPEED 0.25f
#define ENABLE_CORNER_DETECTION false
#define CORNER_SIZE 0.08f

typedef struct {
	float distance;
	bool is_corner;
} Ray_cast_result;

typedef struct {
	Position position;
	float direction;
} Entity;

static bool input_thread_running;
static char last_input;

void draw_map_and_player_into_screen_buffer(Map* map, Entity player, Screen* screen);
void move_entity_on_map(Entity* entity, Map* map, float direction, float distance);
bool is_position_a_corner(Position position);
Ray_cast_result cast_ray(Position position, float direction, Map* map);
char* get_shade_by_distance(float distance);
void* run_inpu_thread();
int main(int argc, char** argv);

#endif

