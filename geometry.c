
#include <math.h>

#include "geometry.h"

float get_distance_between_positions(Position p1, Position p2) {
	return sqrt(powf(p1.x - p2.x, 2) + powf(p1.y - p2.y, 2));
}

Position apply_vector_to_position(Vector vector, Position position) {
	return (Position) {
			position.x + sin(vector.direction) * vector.length,
			position.y + cos(vector.direction) * vector.length};
}

