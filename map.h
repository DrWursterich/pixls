
#ifndef MAP_C
#define MAP_C

#define MAP_TILE_EMPTY '.'
#define MAP_TILE_WALL '#'

#include "geometry.h"

typedef struct {
	int width;
	int height;
	int buffer_length;
	char buffer[];
} Map;

Map* create_map();
void destroy_map(Map* map);
char get_map_tile_of_position(Map* map, Position position);
bool position_is_on_map(Position position, Map* map);

#endif

